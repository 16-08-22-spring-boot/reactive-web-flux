package com.classpath.orders.service;

import org.springframework.stereotype.Service;

import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class OrderService {
		
	private final OrderRepository orderRepository;
	
	public Mono<Order> saveOrder(Order order) {
		Mono<Order> savedOrder = this.orderRepository.save(order);
		return savedOrder;
	}
	
	public Flux<Order> fetchAllOrders(){
		return this.orderRepository.findAll();
	}
	
	public Mono<Order> findByOrderId(long orderId){
		Mono<Order> orderMono = this.orderRepository.findById(orderId)
													.switchIfEmpty(Mono.error(new IllegalArgumentException("invalid order id passed")));
		return orderMono;
	}
	
	public Mono<Order> deleteOrderById(long orderId){
		Mono<Order> monoOrder = this.orderRepository.findById(orderId);
		//Mono<Mono<Void>> map = monoOrder.map(order -> this.orderRepository.delete(order));
		Mono<Order> deletedOrder = monoOrder.flatMap(order -> this.orderRepository.delete(order).thenReturn(order));
		return deletedOrder;
	}
}
