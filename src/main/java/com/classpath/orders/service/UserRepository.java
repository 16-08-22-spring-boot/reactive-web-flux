package com.classpath.orders.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class UserRepository {
	
	private final PasswordEncoder passwordEncoder;
	
	public Mono<UserDetails> findByUsername(String username) {
		if (username.equalsIgnoreCase("kiran")) {
			UserDetails kiran = User
								.builder()
									.username("kiran")
									.password(this.passwordEncoder.encode("welcome"))
									.roles("USER")
									.build();
			return Mono.just(kiran);
		} else if(username.equalsIgnoreCase("vinay")){
			UserDetails vinay = User
					.builder()
						.username("vinay")
						.password(this.passwordEncoder.encode("welcome"))
						.roles("USER", "ADMIN")
						.build();
			return Mono.just(vinay);
		} else {
			return Mono.error(() -> new UsernameNotFoundException("invalid username/password"));
		}
	}

}
