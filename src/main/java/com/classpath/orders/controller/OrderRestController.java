package com.classpath.orders.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.orders.model.Order;
import com.classpath.orders.service.OrderService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
@Slf4j
public class OrderRestController {
	
	private final OrderService orderService;
	
	@GetMapping
	public Flux<Order> fetchOrders(){
		return this.orderService.fetchAllOrders();
	}
	
	@GetMapping("/{id}")
	public Mono<Order> fetchOrderByOrderId(@PathVariable("id") long orderId){
		return this.orderService.findByOrderId(orderId);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Mono<Order> saveOrder(@RequestBody @Valid Order order){
		return this.orderService.saveOrder(order);
	}
	
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public Mono<Order> deleteOrderByOrderId(@PathVariable("id") long orderId){
		return this.orderService.deleteOrderById(orderId);
	}


}
