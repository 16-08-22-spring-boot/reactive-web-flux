package com.classpath.orders.config;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;
import com.github.javafaker.Faker;
import com.github.javafaker.Name;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class BootstrapOrders {
	
	private final OrderRepository orderRepository;
	private final Faker faker = new Faker();
	
	@EventListener(ApplicationReadyEvent.class)
	public void insertOrders(ApplicationReadyEvent readyEvent) {
		log.info("================== inserting orders data to the db ====================");
		IntStream.range(1, 100)
			.forEach((index) -> {
				Name name = faker.name();
				Order order = Order.builder().email(name.firstName()+ "@"+ faker.internet().domainName())
				.name(name.firstName())
				.orderDate(faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
				.build();
				Mono<Order> savedOrder = this.orderRepository.save(order);
				savedOrder.subscribe(o -> System.out.println("Saved "+ o.getOrderId()+ " to the database"));
			});
	}

}
