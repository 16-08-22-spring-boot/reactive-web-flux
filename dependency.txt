[INFO] Scanning for projects...
[INFO] 
[INFO] -----------------< com.classpath:orders-api-reactive >------------------
[INFO] Building orders-api-reactive 0.0.1-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-dependency-plugin:3.3.0:tree (default-cli) @ orders-api-reactive ---
[INFO] com.classpath:orders-api-reactive:jar:0.0.1-SNAPSHOT
[INFO] +- org.springframework.boot:spring-boot-starter-data-r2dbc:jar:2.7.1:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter:jar:2.7.1:compile
[INFO] |  |  +- org.springframework.boot:spring-boot:jar:2.7.1:compile
[INFO] |  |  +- org.springframework.boot:spring-boot-autoconfigure:jar:2.7.1:compile
[INFO] |  |  +- org.springframework.boot:spring-boot-starter-logging:jar:2.7.1:compile
[INFO] |  |  |  +- ch.qos.logback:logback-classic:jar:1.2.11:compile
[INFO] |  |  |  |  \- ch.qos.logback:logback-core:jar:1.2.11:compile
[INFO] |  |  |  +- org.apache.logging.log4j:log4j-to-slf4j:jar:2.17.2:compile
[INFO] |  |  |  |  \- org.apache.logging.log4j:log4j-api:jar:2.17.2:compile
[INFO] |  |  |  \- org.slf4j:jul-to-slf4j:jar:1.7.36:compile
[INFO] |  |  +- jakarta.annotation:jakarta.annotation-api:jar:1.3.5:compile
[INFO] |  |  \- org.yaml:snakeyaml:jar:1.30:compile
[INFO] |  +- org.springframework.data:spring-data-r2dbc:jar:1.5.1:compile
[INFO] |  |  +- org.springframework.data:spring-data-commons:jar:2.7.1:compile
[INFO] |  |  +- org.springframework.data:spring-data-relational:jar:2.4.1:compile
[INFO] |  |  +- org.springframework:spring-r2dbc:jar:5.3.21:compile
[INFO] |  |  +- org.springframework:spring-tx:jar:5.3.21:compile
[INFO] |  |  +- org.springframework:spring-context:jar:5.3.21:compile
[INFO] |  |  |  +- org.springframework:spring-aop:jar:5.3.21:compile
[INFO] |  |  |  \- org.springframework:spring-expression:jar:5.3.21:compile
[INFO] |  |  +- org.springframework:spring-beans:jar:5.3.21:compile
[INFO] |  |  \- org.slf4j:slf4j-api:jar:1.7.36:compile
[INFO] |  +- io.r2dbc:r2dbc-spi:jar:0.9.1.RELEASE:compile
[INFO] |  |  \- org.reactivestreams:reactive-streams:jar:1.0.4:compile
[INFO] |  \- io.r2dbc:r2dbc-pool:jar:0.9.0.RELEASE:compile
[INFO] |     \- io.projectreactor.addons:reactor-pool:jar:0.2.9:compile
[INFO] +- org.springframework.boot:spring-boot-starter-webflux:jar:2.7.1:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-json:jar:2.7.1:compile
[INFO] |  |  +- com.fasterxml.jackson.core:jackson-databind:jar:2.13.3:compile
[INFO] |  |  |  +- com.fasterxml.jackson.core:jackson-annotations:jar:2.13.3:compile
[INFO] |  |  |  \- com.fasterxml.jackson.core:jackson-core:jar:2.13.3:compile
[INFO] |  |  +- com.fasterxml.jackson.datatype:jackson-datatype-jdk8:jar:2.13.3:compile
[INFO] |  |  +- com.fasterxml.jackson.datatype:jackson-datatype-jsr310:jar:2.13.3:compile
[INFO] |  |  \- com.fasterxml.jackson.module:jackson-module-parameter-names:jar:2.13.3:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-reactor-netty:jar:2.7.1:compile
[INFO] |  |  \- io.projectreactor.netty:reactor-netty-http:jar:1.0.20:compile
[INFO] |  |     +- io.netty:netty-codec-http:jar:4.1.78.Final:compile
[INFO] |  |     |  +- io.netty:netty-common:jar:4.1.78.Final:compile
[INFO] |  |     |  +- io.netty:netty-buffer:jar:4.1.78.Final:compile
[INFO] |  |     |  +- io.netty:netty-transport:jar:4.1.78.Final:compile
[INFO] |  |     |  +- io.netty:netty-codec:jar:4.1.78.Final:compile
[INFO] |  |     |  \- io.netty:netty-handler:jar:4.1.78.Final:compile
[INFO] |  |     +- io.netty:netty-codec-http2:jar:4.1.78.Final:compile
[INFO] |  |     +- io.netty:netty-resolver-dns:jar:4.1.78.Final:compile
[INFO] |  |     |  +- io.netty:netty-resolver:jar:4.1.78.Final:compile
[INFO] |  |     |  \- io.netty:netty-codec-dns:jar:4.1.78.Final:compile
[INFO] |  |     +- io.netty:netty-resolver-dns-native-macos:jar:osx-x86_64:4.1.78.Final:compile
[INFO] |  |     |  \- io.netty:netty-resolver-dns-classes-macos:jar:4.1.78.Final:compile
[INFO] |  |     +- io.netty:netty-transport-native-epoll:jar:linux-x86_64:4.1.78.Final:compile
[INFO] |  |     |  +- io.netty:netty-transport-native-unix-common:jar:4.1.78.Final:compile
[INFO] |  |     |  \- io.netty:netty-transport-classes-epoll:jar:4.1.78.Final:compile
[INFO] |  |     \- io.projectreactor.netty:reactor-netty-core:jar:1.0.20:compile
[INFO] |  |        \- io.netty:netty-handler-proxy:jar:4.1.78.Final:compile
[INFO] |  |           \- io.netty:netty-codec-socks:jar:4.1.78.Final:compile
[INFO] |  +- org.springframework:spring-web:jar:5.3.21:compile
[INFO] |  \- org.springframework:spring-webflux:jar:5.3.21:compile
[INFO] +- com.h2database:h2:jar:2.1.214:runtime
[INFO] +- io.r2dbc:r2dbc-h2:jar:0.9.1.RELEASE:runtime
[INFO] |  \- io.projectreactor:reactor-core:jar:3.4.19:compile
[INFO] +- org.projectlombok:lombok:jar:1.18.24:compile
[INFO] +- org.springframework.boot:spring-boot-starter-test:jar:2.7.1:test
[INFO] |  +- org.springframework.boot:spring-boot-test:jar:2.7.1:test
[INFO] |  +- org.springframework.boot:spring-boot-test-autoconfigure:jar:2.7.1:test
[INFO] |  +- com.jayway.jsonpath:json-path:jar:2.7.0:test
[INFO] |  |  \- net.minidev:json-smart:jar:2.4.8:test
[INFO] |  |     \- net.minidev:accessors-smart:jar:2.4.8:test
[INFO] |  |        \- org.ow2.asm:asm:jar:9.1:test
[INFO] |  +- jakarta.xml.bind:jakarta.xml.bind-api:jar:2.3.3:test
[INFO] |  |  \- jakarta.activation:jakarta.activation-api:jar:1.2.2:test
[INFO] |  +- org.assertj:assertj-core:jar:3.22.0:test
[INFO] |  +- org.hamcrest:hamcrest:jar:2.2:test
[INFO] |  +- org.junit.jupiter:junit-jupiter:jar:5.8.2:test
[INFO] |  |  +- org.junit.jupiter:junit-jupiter-api:jar:5.8.2:test
[INFO] |  |  |  +- org.opentest4j:opentest4j:jar:1.2.0:test
[INFO] |  |  |  +- org.junit.platform:junit-platform-commons:jar:1.8.2:test
[INFO] |  |  |  \- org.apiguardian:apiguardian-api:jar:1.1.2:test
[INFO] |  |  +- org.junit.jupiter:junit-jupiter-params:jar:5.8.2:test
[INFO] |  |  \- org.junit.jupiter:junit-jupiter-engine:jar:5.8.2:test
[INFO] |  |     \- org.junit.platform:junit-platform-engine:jar:1.8.2:test
[INFO] |  +- org.mockito:mockito-core:jar:4.5.1:test
[INFO] |  |  +- net.bytebuddy:byte-buddy:jar:1.12.11:test
[INFO] |  |  +- net.bytebuddy:byte-buddy-agent:jar:1.12.11:test
[INFO] |  |  \- org.objenesis:objenesis:jar:3.2:test
[INFO] |  +- org.mockito:mockito-junit-jupiter:jar:4.5.1:test
[INFO] |  +- org.skyscreamer:jsonassert:jar:1.5.0:test
[INFO] |  |  \- com.vaadin.external.google:android-json:jar:0.0.20131108.vaadin1:test
[INFO] |  +- org.springframework:spring-core:jar:5.3.21:compile
[INFO] |  |  \- org.springframework:spring-jcl:jar:5.3.21:compile
[INFO] |  +- org.springframework:spring-test:jar:5.3.21:test
[INFO] |  \- org.xmlunit:xmlunit-core:jar:2.9.0:test
[INFO] \- io.projectreactor:reactor-test:jar:3.4.19:test
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  7.998 s
[INFO] Finished at: 2022-07-20T11:49:56+05:30
[INFO] ------------------------------------------------------------------------
